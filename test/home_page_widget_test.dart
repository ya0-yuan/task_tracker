import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:task_tracker/HomePage.dart';

import 'package:task_tracker/main.dart';

void main() {
  testWidgets('home page contains All tasks text',
          (WidgetTester tester) async {
        final testWidget = MaterialApp(
          home: HomePage(),
        );

        await tester.pumpWidget(testWidget);
        await tester.pumpAndSettle();

        expect(find.text('All tasks'), findsOneWidget);
      });

  testWidgets('home page contains calendar icon button', (WidgetTester tester) async {
    final testWidget = MaterialApp(
      home: HomePage(),
    );

    await tester.pumpWidget(testWidget);
    await tester.pumpAndSettle();

    expect(find.byType(IconButton), findsOneWidget);
    expect(find.byKey(Key('icon_calendar')), findsOneWidget);
  });

  testWidgets('home page contains add button', (WidgetTester tester) async {
    final testWidget = MaterialApp(
      home: HomePage(),
    );

    await tester.pumpWidget(testWidget);
    await tester.pumpAndSettle();

    expect(find.byType(IconButton), findsOneWidget);
    expect(find.byKey(Key('icon_add')), findsOneWidget);
  });
}
