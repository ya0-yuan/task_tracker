import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TaskList extends StatefulWidget {
  @override
  _TaskListState createState() => _TaskListState();
}

class _TaskListState extends State<TaskList> {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      physics: const AlwaysScrollableScrollPhysics(),
      shrinkWrap: true,
      itemBuilder: (context, index) {
        return ListTile(
            onTap: null,
            leading: Container(
              padding: EdgeInsets.all(2),
              height: 30,
              width: 30,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: Colors.blue
              ),
              child: Icon(Icons.check,
                color: Colors.white,
                key: Key('icon_check'),
              ),
            ),
        title: Text('task here',
          style: TextStyle(
            fontSize: 20,
          ),
        ),
        subtitle: Text(
            "description \ndate",
            style: TextStyle(
              fontSize: 15,
              color: Colors.grey[400],
            ),
          ),
          isThreeLine: true,
        );
      },
    );
  }
}
