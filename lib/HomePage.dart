import 'package:flutter/material.dart';
import 'package:task_tracker/Tasklist.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('All tasks'),
        actions: [
          IconButton(
            icon: Icon(Icons.calendar_today,
              color: Colors.white,
              key: Key('icon_calendar'),
            ),
            onPressed: null,
          )
        ],
      ),
      body: TaskList(),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        key: Key('icon_add'),
        backgroundColor: Theme.of(context).primaryColor,
        onPressed: null,
      ),
    );
  }
}
